package com.example.SoupServiceApp.services;

import com.example.SoupServiceApp.models.Covid;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class CovidService {
    private static final Covid[] covids = new Covid[] {
        new Covid("US", 760245, 40702, 71003),
        new Covid("Spain", 200210, 166256,635895),
        new Covid("Italy", 178072, 23660, 51003)
    };
    @WebMethod
    public Covid[] getStatistics() {
        return covids;
    }

    @WebMethod
    public Covid getCountryStatistic(String country) {
        for (Covid covid : covids) {
            if (covid.getCountry().equals(country)) {
                return covid;
            }
        }
        return null;
    }

    @WebMethod
    public int getTotalConfirmed() {
        int totalConfirmed = 0;
        for (Covid covid : covids) {
            totalConfirmed += covid.getConfirmed();
        }
        return totalConfirmed;
    }

    @WebMethod
    public int getTotalDeath() {
        int totalDeath = 0;
        for (Covid covid : covids) {
            totalDeath += covid.getDeaths();
        }
        return totalDeath;
    }

    @WebMethod
    public int getTotalRecovered() {
        int totalRecovered = 0;
        for (Covid covid : covids) {
            totalRecovered += covid.getRecovered();
        }
        return totalRecovered;
    }
}
